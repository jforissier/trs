About TRS
#########
Developing software on its own is complicated and requires time, skills and lots
of efforts. But being good at writing individual software isn't sufficient in
this day and age. Systems are inherently complicated with lots of components
interacting with each other. We have to deal with intra communication as well as
external communication with remote systems. All aspects of security has to be
considered, standards needs to be addressed and systems needs to be tested not
only as individual components, but as coherent systems. For device manufacturers
this becomes a real challenge, which is very costly both in terms on time and
effort.

As an answer to the challenges presented, Linaro have created **TRS (Trusted
Reference Stack)**, which is an umbrella project and a software stack containing
well tested software components making up a solid base for efficient development
and for building unique and differentiating end-to-end use cases.

.. _about_goals:

Goals and key properties
************************
* Common platform for deliverables from Linaro.
* Include all Linaro test suites and test frameworks making CI/CD and regression
  testing fast, valuable and efficient.
* Efficient development environment for engineers.
* A product ready reference implementation.
* Configurable to be able to meet different needs.
* Common ground and building block for Blueprints and similar targets.
* Interoperability making it possible to use alternative implementations.
* Pre-silicon IP support in environments like QEMU etc.

Firmware Software Components
****************************
The firmware components for TRS is provided by 
`Trusted Substrate <https://trusted-substrate.readthedocs.io/en/latest/>`_. For
more details, please look `here
<https://trusted-substrate.readthedocs.io/en/latest/requirements/supported_platforms.html#software-components>`_.

Releases
********

v0.1-beta - 2022-09-02
~~~~~~~~~~~~~~~~~~~~~~
#. Builds TRS for the QEMU target.
#. Boot cleanly up to the login prompt.
#. Nothing tested.
#. RockPi4 works, but not officially part of the v0.1-beta release.
